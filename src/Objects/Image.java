package Objects;

import java.io.Serializable;
import java.util.List;

public class Image implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> tags;
	private int counter_comments;
	private int counter_likes;
	private String img_url;
	private String time;
	private long latitude;
	private long longitude;
	private User user;
	
	public Image(){
		
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public int getCounter_comments() {
		return counter_comments;
	}

	public void setCounter_comments(int counter_comments) {
		this.counter_comments = counter_comments;
	}

	public int getCounter_likes() {
		return counter_likes;
	}

	public void setCounter_likes(int counter_likes) {
		this.counter_likes = counter_likes;
	}

	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public long getLatitude() {
		return latitude;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public long getLongitude() {
		return longitude;
	}

	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
