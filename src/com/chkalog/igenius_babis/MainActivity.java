package com.chkalog.igenius_babis;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import API.JSONParser;
import API.WSGetPhotos;
import Instagram.InstagramApp;
import Instagram.InstagramApp.OAuthAuthenticationListener;
import Instagram.InstagramSession;
import Objects.GridViewAdapter;
import Objects.Image;
import Objects.User;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class MainActivity extends ActionBarActivity {

	static Button btnConnect;
	
	private static InstagramApp mApp;
	InstagramSession mSession;
	
	private GridView gridView;
    private GridViewAdapter gridAdapter;
    
    List<Image> insta_images;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mApp = new InstagramApp(MainActivity.this, Application.Application.CLIENT_ID,
				Application.Application.CLIENT_SECRET, Application.Application.CALLBACK_URL);
		mApp.setListener(listener);
		
		mSession = new InstagramSession(MainActivity.this);
		
		btnConnect = (Button) findViewById(R.id.btnConnect);
		
		insta_images = new ArrayList<Image>();
		
		gridView = (GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, insta_images);
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        		Image item = (Image) parent.getItemAtPosition(position);
        		//Create intent
        		Intent intent = new Intent(MainActivity.this, FullscreenActivity.class);

        		Bundle bundle = new Bundle();
        		bundle.putSerializable("image", item);
        		intent.putExtras(bundle);
        		
        		//Start details activity
        		startActivity(intent);
        	}
        });
	}
	
	public void OAUTH(View view){
		if (mApp.hasAccessToken()) {
			logout();
		} else {
			mApp.authorize();
		}
	}

	private void logout(){
		final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setMessage("Disconnect from Instagram?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialog, int id) {
								mApp.resetAccessToken();
								btnConnect.setText("Connect");
								btnConnect.setVisibility(View.VISIBLE);
								gridView.setVisibility(View.GONE);
								insta_images.clear();
							}
						})
				.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		final AlertDialog alert = builder.create();
		alert.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	MenuItem logout;
	public boolean onPrepareOptionsMenu(Menu menu)
	{
	    logout = menu.findItem(R.id.action_logout);      
	    logout.setVisible(mApp.hasAccessToken());
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		switch (item.getItemId()) {
		    case R.id.action_logout:            
		    	logout();
		        return true;        
		    default:
		        return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onResume(){
	    super.onResume();
		if(mApp.hasAccessToken()){
			setTitle(mApp.getUserName());
			attemptPhotos();
		}
	}
	
	private void attemptPhotos(){
		if(Application.Application.isconnected(MainActivity.this))
			new AttemptGetPhotos(MainActivity.this, mSession.getAccessToken()).execute();
		else
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_internet_connection), Toast.LENGTH_SHORT).show();
	}
	
	OAuthAuthenticationListener listener = new OAuthAuthenticationListener() {

		@Override
		public void onSuccess() {
			//tvSummary.setText("Connected as " + mApp.getUserName());
			setTitle(mApp.getUserName());
			btnConnect.setText("Disconnect");
			if(mApp.hasAccessToken())
				attemptPhotos();
		}

		@Override
		public void onFail(String error) {
			Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
		}
	};
	
	
	private String WSGetPhotos_url = "https://api.instagram.com/v1/users/self/media/recent?access_token=";
	
	class AttemptGetPhotos extends WSGetPhotos {
		private String accessToken;
		List<Image> images;
		public AttemptGetPhotos(Context ctx, String token) {
			super(ctx, token);
			this.accessToken = token;
		}
		
		private ProgressDialog pDialog;
		
		@Override
		protected void onPreExecute() {
			this.pDialog = new ProgressDialog(MainActivity.this);
			this.pDialog.setMessage(getResources().getString(R.string.hint_get_photos));
			this.pDialog.setIndeterminate(false);
			this.pDialog.setCancelable(true);
			this.pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			JSONParser jsonParser = new JSONParser();
			
			try {
				JSONObject json = jsonParser.makeHttpRequest(WSGetPhotos_url+this.accessToken, "GET", null);
				Log.d("json", json.toString());
				// json success tag
				images = new ArrayList<Image>();
				images.clear();
				JSONArray data = json.getJSONArray("data");
				for(int i=0;i<data.length();i++){
					
					JSONObject json_image = data.getJSONObject(i);
					
					if(json_image.getString("type").equals("image")){
						JSONArray tags = json_image.getJSONArray("tags");
						
						String created_time = json_image.getString("created_time");
						
						List<String> list_tags = new ArrayList<String>();
						for(int l=0;l<tags.length();l++){
							list_tags.add(tags.getString(l));
						}
						
						long latitude = 0;
						long longitude = 0;
						
						try{
							JSONObject json_location = json_image.getJSONObject("location");
							latitude = json_location.getLong("latitude");
							longitude = json_location.getLong("longitude");
						}catch(JSONException j){
							
						}
						
						JSONObject json_comments = json_image.getJSONObject("comments");
						int comments_counter = json_comments.getInt("count");
						
						JSONObject json_likes = json_image.getJSONObject("likes");
						int likes_counter = json_likes.getInt("count");
						
						JSONObject json_images = json_image.getJSONObject("images");
						JSONObject json_images_standard = json_images.getJSONObject("standard_resolution");
						String image_url = json_images_standard.getString("url");
						
						JSONObject json_user = json_image.getJSONObject("user");
						String username = json_user.getString("username");
						String profile_picture = json_user.getString("profile_picture");
						String id = json_user.getString("id");
						String full_name = json_user.getString("full_name");
						
						Image image = new Image(); 
						image.setCounter_comments(comments_counter);
						image.setCounter_likes(likes_counter);
						image.setImg_url(image_url);
						image.setLatitude(latitude);
						image.setLongitude(longitude);
						image.setTags(list_tags);
						image.setTime(created_time);
						image.setUser(new User());
						image.getUser().setFull_name(full_name);
						image.getUser().setId(id);
						image.getUser().setProfile_picture(profile_picture);
						image.getUser().setUsername(username);
						
						images.add(image);
					}
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "KO";
			}
			
			return "OK";
		}
		
		protected void onPostExecute(String RES_CODE) {
		    // dismiss the dialog once product deleted
			this.pDialog.dismiss();
		    if (RES_CODE.equals("OK")){

				insta_images.clear();
				insta_images.addAll(images);
				
				//ELSE CASE ==> PAGINATION
				
				gridAdapter.notifyDataSetChanged();
				btnConnect.setVisibility(View.GONE);
				logout.setVisible(true);
		    }else{
		    	Toast.makeText(getApplicationContext(), getResources().getString(R.string.general_err), Toast.LENGTH_LONG).show();
		    }
		}
	}
}
