package Application;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Application {
	
	public static String CLIENT_ID 		= "1ffa2044c0f8485c9be91d7ad6ab7326";
	public static String CLIENT_SECRET 	= "642d4134c14f49d1a01ddce5343676fa";
	public static String CALLBACK_URL  	= "http://www.belocale.com/";
	public static String SUPPORT_EMAIL 	= "charalampos.kalogiannakis@gmail.com";
	
	public static boolean isconnected(Context context){
    	ConnectivityManager cm =
    	        (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	 
    	NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    	return (activeNetwork != null &&
    	                      activeNetwork.isConnectedOrConnecting());
    }
}
